//https://api.openweathermap.org/data/2.5/weather?q={city name}&appid={API key}
//http://openweathermap.org/img/wn/10d@2x.png
const form = document.querySelector(".top-banner form");
const input = document.querySelector(".top-banner input");
const msg = document.querySelector(".msg");
const cities = document.querySelector(".cities");
form.addEventListener("submit", (e) => {
    e.preventDefault();
    let inputVal = input.value;
    // console.log(inputVal);
    const apiKey = "49851d6d8e1e278c28eb6f484aafe1c5";
    let url =  `https:api.openweathermap.org/data/2.5/weather?q=${inputVal}&appid=${apiKey}&units=metric `;
    // console.log(url);
    fetch(url)
        .then(res => res.json())
        .then(data => {
            // console.log(data);
            const {main, name, sys, weather}= data;
            // console.log(weather);
            // console.log(name);
            // console.log(sys);
            // console.log(weather);
            const city = document.createElement("li");
            city.classList.add("city");
            let temp = Math.floor(main.temp); 
            let icon = `http://openweathermap.org/img/wn/${weather[0].icon}.png`
            console.log(temp);

            const markUp = 
            `<h2 class ="city-name">
                <span>${name}</span>
                <span>${sys.country}</span>
            </h2>
            <div class = "city-temp">${temp}</div>
            <figure>
                <img class ="city-icon" src='${icon}'>
                <figurecaption>${weather[0].description}</figurecaption>
            </figure>`;
            city.innerHTML = markUp;
            cities.appendChild(city);
            msg.innerText = "";
         
        })
        .catch(() => {
            msg.innerText = "Search for a valid city";
        })
        input.value = "";
})
